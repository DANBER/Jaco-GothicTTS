import json
import os

import tqdm

import action_tts

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
texts_path = file_path + "../userdata/my_texts.json"


# ==================================================================================================


def main():
    with open(texts_path, "r", encoding="utf-8") as json_file:
        texts = json.load(json_file)

    for text in tqdm.tqdm(texts):
        _ = action_tts.create_and_read(text)


# ==================================================================================================

if __name__ == "__main__":
    main()
